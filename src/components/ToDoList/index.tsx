import { List } from '@mui/material'
import React from 'react'
import { ToDo } from '../../types'
import ToDoItem from '../ToDoItem'

interface ToDoListProps {
  children?: React.ReactNode
  todos?: ToDo[]
}

export default function ToDoList({ children, todos }: ToDoListProps) {
  return (
    <List>
      {todos?.map((todo: ToDo) => (
        <ToDoItem
          key={todo._id}
          _id={todo._id}
          text={todo.text}
          created_at={todo.created_at}
          done={todo.done}
        />
      ))}
    </List>
  )
}
