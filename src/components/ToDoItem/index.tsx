import React from 'react'
import { ListItem, ListItemText, Button } from '@mui/material'
import { useDispatch } from 'react-redux'
import { markDone } from '../../slices/todo.slice'

interface ToDoItemProps {
  _id: number
  text: string
  done: boolean
  created_at: string
}

export default function ToDoItem({
  text,
  done,
  created_at,
  _id,
}: ToDoItemProps) {
  const dispatch = useDispatch()
  return (
    <ListItem>
      <ListItemText sx={{ textDecoration: done ? 'line-through' : '' }}>
        {text}
      </ListItemText>
      <Button
        variant="contained"
        onClick={() => dispatch(markDone({ _id, status: true }))}
      >
        Done
      </Button>
    </ListItem>
  )
}
