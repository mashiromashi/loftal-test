export type ToDo = {
  _id: number
  text: string
  done: boolean
  created_at: string
}
