import { ToDo } from '../types'
import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import moment from 'moment'
interface TodoInitState {
  todos: ToDo[]
}

const initState: TodoInitState = {
  todos: [
    {
      _id: 1,
      done: false,
      text: 'Eat Breakfast',
      created_at: moment().format('DD/MM/YYYY'),
    },
  ],
}

export const TodoSlice = createSlice({
  name: 'todo',
  initialState: initState,
  reducers: {
    addTodo: (state, action: PayloadAction<ToDo>) => {
      state.todos.push(action.payload)
    },
    markDone: (
      state,
      action: PayloadAction<{ _id: number; status: boolean }>,
    ) => {
      const toBeMarked = state.todos.findIndex(
        (todo) => todo._id === action.payload._id,
      )
      state.todos[toBeMarked].done = action.payload.status
    },
    removeAllDone: (state) => {
      const notDone = state.todos.filter((todo) => todo.done === false)
      state.todos = notDone
    },
  },
})

export const { addTodo, markDone, removeAllDone } = TodoSlice.actions
export default TodoSlice.reducer
