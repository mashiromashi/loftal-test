import ToDoList from './components/ToDoList'
import moment from 'moment'
import { Box, TextField, Button, Container, Typography } from '@mui/material'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from './store'
import { addTodo, removeAllDone } from './slices/todo.slice'

function App() {
  const todos = useSelector((state: RootState) => state.todo.todos)
  const [todoText, setTodoText] = React.useState<string>('')
  const dispatch = useDispatch()
  const handleOnAdd = (event: any) => {
    dispatch(
      addTodo({
        _id: Math.floor(Math.random() * 1000),
        created_at: moment().format('DD/MM/YYYY'),
        text: todoText,
        done: false,
      }),
    )
    setTodoText('')
  }
  return (
    <React.Fragment>
      <Container maxWidth="sm">
        <Box display="flex" justifyContent="center">
          <TextField
            sx={{ mr: 1 }}
            placeholder="Enter Text"
            onChange={(event) => {
              event.preventDefault()
              setTodoText(event.currentTarget.value)
            }}
          />
          <Button variant="contained" onClick={handleOnAdd}>
            Add
          </Button>
          <Button
            variant="contained"
            onClick={() => dispatch(removeAllDone())}
            sx={{ ml: 1 }}
          >
            Delete All Done
          </Button>
        </Box>
        <Typography mt={4} variant="h6" textAlign={'center'}>
          To Do List
        </Typography>
        <ToDoList todos={todos} />
      </Container>
    </React.Fragment>
  )
}

export default App
